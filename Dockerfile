FROM adoptopenjdk/openjdk11:alpine
EXPOSE 8090
#COPY src/main/resources/ /app
COPY target/quest-spring-boot.jar /app/quest-spring-boot.jar
WORKDIR /app
CMD ["java","-jar","quest-spring-boot.jar","--spring.profiles.active=production"]
