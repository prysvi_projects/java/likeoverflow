package com.quest.etna.dto;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.quest.etna.model.ValidationUnitActivity;
import lombok.*;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ValidationUnitDTO {
    private Long id;
    private String name;
    private String longName;
    private List<ValidationUnitActivityDTO> validationUnitActivityList;
}
