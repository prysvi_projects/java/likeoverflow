package com.quest.etna.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddressDTO {

    private Integer id;

    private String street;

    private String postalCode;

    private String city;

    private String country;

    private UserDTO user;

    private Date creationDate;

    private Date updatedDate;
}
