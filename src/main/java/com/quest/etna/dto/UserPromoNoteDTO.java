package com.quest.etna.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserPromoNoteDTO {
    private UserDTO user;
    private Double note;
}
