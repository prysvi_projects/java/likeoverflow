package com.quest.etna.security.oauth;

import com.quest.etna.config.AppProperties;
import com.quest.etna.exception.CustomAuthenticationException;
import com.quest.etna.model.JwtUserDetails;
import com.quest.etna.model.User;
import com.quest.etna.security.jwt.JwtTokenUtil;
import com.quest.etna.security.jwt.JwtUserDetailsService;
import com.quest.etna.service.EtnaService;
import com.quest.etna.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.util.Optional;

@RequiredArgsConstructor
@Component
public class OAuth2AuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

    public static final String REDIRECT_URI_PARAM_COOKIE_NAME = "redirect_uri";
    private final JwtUserDetailsService jwtUserDetailsService;
    private final JwtTokenUtil jwtTokenUtil;
    private final HttpCookieOAuth2AuthorizationRequestRepository httpCookieOAuth2AuthorizationRequestRepository;
    private final UserService userService;
    private final BCryptPasswordEncoder passwordEncoder;
    private final EtnaService etnaService;
    private final AppProperties appProperties;


    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        String targetUrl = determineTargetUrl(request, response, authentication);

        if (response.isCommitted()) {
            logger.debug("Response has already been committed. Unable to redirect to " + targetUrl);
            return;
        }

        clearAuthenticationAttributes(request, response);
        getRedirectStrategy().sendRedirect(request, response, targetUrl);
    }

    protected String determineTargetUrl(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        Optional<String> redirectUri = Optional.ofNullable(appProperties.getOauth2().getTokenSuccessRedirectUrl());
        String targetUrl = redirectUri.orElse(getDefaultTargetUrl());

        DefaultOidcUser authenticationPrincipal = (DefaultOidcUser) authentication.getPrincipal();
        String username = authenticationPrincipal.getAttribute("email");
        if (null == username) {
            throw new CustomAuthenticationException("Sorry, the provider did not return enough information for your" +
                    " authorization. Please contact the site administrator");
        }
        User foundUser = etnaService.getByEmail(username);
        if (null == foundUser) {
            throw new CustomAuthenticationException("Sorry, something wrong. Please contact the site administrator");
        }
        if (!foundUser.isEnabled()) {
            foundUser.setEnabled(true);
            foundUser = userService.update(foundUser, false);
        }
        String token = jwtTokenUtil.generateToken(new JwtUserDetails(foundUser));

        return UriComponentsBuilder.fromUriString(targetUrl + "/" + token)
                //.queryParam("token", token)
                .build().toUriString();
    }

    protected void clearAuthenticationAttributes(HttpServletRequest request, HttpServletResponse response) {
        super.clearAuthenticationAttributes(request);
        httpCookieOAuth2AuthorizationRequestRepository.removeAuthorizationRequestCookies(request, response);
    }


}
