package com.quest.etna.security.jwt;

import com.quest.etna.model.JwtUserDetails;
import com.quest.etna.security.UserBasedAuthenticationToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JwtRequestFilter extends OncePerRequestFilter {

    @Autowired
    private JwtUserDetailsService jwtUserDetailsService;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {

        String token = request.getHeader("Authorization");
        try {
            if (token != null) {
                token = token.replace("Bearer ", "");
                JwtUserDetails userDetails = (JwtUserDetails) jwtUserDetailsService
                        .loadUserByUsername(jwtTokenUtil.getUsernameFromToken(token));

                if (jwtTokenUtil.validateToken(token, userDetails)) {
                    SecurityContextHolder.getContext().setAuthentication(new UserBasedAuthenticationToken(userDetails));
                }

            }
        } catch (Exception e) {
            SecurityContextHolder.clearContext();
            //response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "");
            //throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,"");
        }
        filterChain.doFilter(request, response);

    }

}
