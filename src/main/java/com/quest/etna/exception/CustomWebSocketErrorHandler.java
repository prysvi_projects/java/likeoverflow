package com.quest.etna.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageDeliveryException;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.socket.messaging.StompSubProtocolErrorHandler;

public class CustomWebSocketErrorHandler extends StompSubProtocolErrorHandler {
    private static final byte[] EMPTY_PAYLOAD = new byte[0];
    private static Logger log = LoggerFactory.getLogger(CustomWebSocketErrorHandler.class);

    public CustomWebSocketErrorHandler() {
    }

    @Override
    public Message<byte[]> handleClientMessageProcessingError(Message<byte[]> clientMessage, Throwable ex) {
        if (ex instanceof MessageDeliveryException) {
            ex = ex.getCause();
        }

        if (ex instanceof AccessDeniedException) {
            return prepareErrorMessage(clientMessage, ex.getMessage(), HttpStatus.UNAUTHORIZED.toString());
        }

        return super.handleClientMessageProcessingError(clientMessage, ex);
    }


    private Message<byte[]> prepareErrorMessage(Message<byte[]> clientMessage, String message, String errorCode) {
        StompHeaderAccessor accessor = StompHeaderAccessor.create(StompCommand.DISCONNECT);
        accessor.setMessage(errorCode);
        accessor.setLeaveMutable(true);
        return MessageBuilder.createMessage(message != null ? message.getBytes() : EMPTY_PAYLOAD, accessor.getMessageHeaders());
    }
}
