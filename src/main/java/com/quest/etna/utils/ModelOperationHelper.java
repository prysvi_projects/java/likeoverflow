package com.quest.etna.utils;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.quest.etna.annotation.ReflectionUpdate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;

@Component
public class ModelOperationHelper {
    public static Logger _log = LoggerFactory.getLogger(ModelOperationHelper.class);

    /**
     * @param fromObj         entity with new values
     * @param toObj           entity where apply the values
     * @param applyNullValues if true, null values will be apply to toObj
     * @param <T>             Object
     */
    public <T> T secureUpdate(T fromObj, T toObj, boolean applyNullValues) {
        Class<?> targetClass = toObj.getClass();
        for (Field field : targetClass.getDeclaredFields()) {
            try {
                field.setAccessible(true);
                if (canModify(field)) {
                    Object fieldValue = field.get(fromObj);
                    if (fieldValue != null || applyNullValues) {
                        //field.set(toObj, field.getType().cast(fieldValue));
                        field.set(toObj, fieldValue);
                    }

                    ReflectionUpdate reflectionUpdate = field.getDeclaredAnnotation(ReflectionUpdate.class);
                    if (null != reflectionUpdate && reflectionUpdate.action().equals(ReflectionUpdate.Action.RECREATE)) {
                        field.set(toObj, field.getType().cast(field.getType().getConstructor().newInstance()));
                    }
                }
            } catch (IllegalAccessException e) {
                throw new RuntimeException("Failed update Field " + field.getName() + ", cant access to field");
            } catch (NoSuchMethodException | InvocationTargetException | InstantiationException e) {
                throw new RuntimeException("Failed recreate Field " + field.getName() +
                        ", because field dont have default constructor. Create it or remove ReflectionUpdate " +
                        "annotation");
            }
        }
        return toObj;
    }

    protected boolean canModify(Field field) {
        if (null == field)
            return false;

        if (Modifier.isFinal(field.getModifiers()))
            return false;

        JsonProperty jsonProperty = field.getDeclaredAnnotation(JsonProperty.class);
        ReflectionUpdate reflectionUpdate = field.getDeclaredAnnotation(ReflectionUpdate.class);

        if (null != jsonProperty && jsonProperty.access().equals(JsonProperty.Access.READ_ONLY)) {
            return false;
        }

        if (null != reflectionUpdate && reflectionUpdate.action().equals(ReflectionUpdate.Action.IGNORE)) {
            return false;
        }
        return true;
    }
}
