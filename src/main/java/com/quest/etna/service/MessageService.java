package com.quest.etna.service;

import com.quest.etna.model.Message;
import com.quest.etna.repository.MessageRepository;
import com.quest.etna.utils.ModelOperationHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.Date;
import java.util.List;

@RequiredArgsConstructor
@Service
public class MessageService {
    private final ModelOperationHelper modelOperationHelper;

    private final MessageRepository messageRepository;

    @Transactional(readOnly = true)
    public Message findById(Long id, boolean throwNotFound) {
        if (!throwNotFound) {
            return messageRepository.findById(id).orElse(null);
        } else {
            return messageRepository.findById(id).orElseThrow(
                    () -> new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Message %d not found",
                            id)));
        }
    }

    public Message create(Message obj) {
        return messageRepository.save(obj);
    }

    public Message update(Message obj) {
        return messageRepository.save(modelOperationHelper.secureUpdate(obj, this.findById(obj.getId(),
                true), false));
    }

    @Transactional(readOnly = true)
    public List<Message> findAll(Pageable pageable) {
        return messageRepository.findAll(pageable).getContent();
    }

    @Transactional(readOnly = true)
    public Page<Message> findAllByQuestionPage(Long questionId, Pageable pageable) {
        return messageRepository.findAllByQuestionId(questionId, pageable);
    }

    @Transactional(readOnly = true)
    public List<Message> findAllByContent(String name, Pageable pageable) {
        return messageRepository.findAllByContentContainsText(name, pageable);
    }

    public boolean delete(long id) {
        try {
            messageRepository.deleteById(id);
            return true;
        } catch (EmptyResultDataAccessException ignored) {
            return false;
        }
    }

    public List<Message> findAllByCreationDateBetween(Date start, Date end) {
        return messageRepository.findAllByCreationDateBetween(start, end);
    }

}
