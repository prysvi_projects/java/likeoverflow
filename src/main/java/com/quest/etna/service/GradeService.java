package com.quest.etna.service;

import com.quest.etna.model.Grade;
import com.quest.etna.repository.GradeRepository;
import com.quest.etna.utils.ModelOperationHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RequiredArgsConstructor
@Service
public class GradeService {
    private final ModelOperationHelper modelOperationHelper;

    private final GradeRepository technologyRepository;

    @Transactional(readOnly = true)
    public Grade findById(Long id, boolean throwNotFound) {
        if (!throwNotFound) {
            return technologyRepository.findById(id).orElse(null);
        } else {
            return technologyRepository.findById(id).orElseThrow(
                    () -> new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Grade %d not found",
                            id)));
        }
    }

    public Grade create(Grade obj) {
        return technologyRepository.save(obj);
    }

    public Grade update(Grade obj) {
        Grade dbGrade = this.findById(obj.getId(), true);
        return technologyRepository.save(modelOperationHelper.secureUpdate(obj, dbGrade, false));
    }

    @Transactional(readOnly = true)
    public List<Grade> findAll(Pageable pageable) {
        return technologyRepository.findAll(pageable).getContent();
    }

    @Transactional(readOnly = true)
    public List<Grade> findAllByName(String name, Pageable pageable) {
        return technologyRepository.findAllByName(name, pageable);
    }

    public boolean delete(long id) {
        try {
            technologyRepository.deleteById(id);
            return true;
        } catch (EmptyResultDataAccessException ignored) {
            return false;
        }
    }


}
