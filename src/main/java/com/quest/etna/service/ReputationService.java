package com.quest.etna.service;

import com.quest.etna.model.Grade;
import com.quest.etna.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class ReputationService {
    private final UserService userService;

    private final GradeService gradeService;

    public void refreshUserReputation(User user, Integer reputation) {
        user.setReputation(user.getReputation() + reputation);
        Grade grade = gradeService.findById((long) (user.getReputation() / 100), false);
        if (null != grade && user.getGrade() != grade) {
            user.setGrade(grade);
            userService.update(user, false);
        }
    }
}
