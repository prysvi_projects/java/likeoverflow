package com.quest.etna.service;

import com.quest.etna.model.ValidationUnit;
import com.quest.etna.model.etna.EtnaNote;
import com.quest.etna.repository.ValidationUnitRepository;
import com.quest.etna.utils.ModelOperationHelper;
import lombok.Data;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
@Data
public class ValidationUnitService {
    private final ModelOperationHelper modelOperationHelper;

    private final ValidationUnitRepository validationUnitRepository;

    @Transactional(readOnly = true)
    public ValidationUnit findById(Long id, boolean throwNotFound) {
        if (!throwNotFound) {
            return validationUnitRepository.findById(id).orElse(null);
        } else {
            return validationUnitRepository.findById(id).orElseThrow(
                    () -> new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("ValidationUnit %d not found",
                            id)));
        }
    }

    @Transactional(readOnly = true)
    public ValidationUnit findByName(String name, boolean throwNotFound) {
        if (!throwNotFound) {
            return validationUnitRepository.findByName(name).orElse(null);
        } else {
            return validationUnitRepository.findByName(name).orElseThrow(
                    () -> new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("ValidationUnit with name %d not found",
                            name)));
        }
    }


    public ValidationUnit create(ValidationUnit obj) {
        return validationUnitRepository.save(obj);
    }

    public ValidationUnit update(ValidationUnit obj) {
        ValidationUnit dbValidationUnit = this.findById(obj.getId(), true);
        return validationUnitRepository.save(modelOperationHelper.secureUpdate(obj, dbValidationUnit, false));
    }

    @Transactional(readOnly = true)
    public List<ValidationUnit> findAll(Pageable pageable) {
        return validationUnitRepository.findAll(pageable).getContent();
    }

    @Transactional(readOnly = true)
    public List<ValidationUnit> findAllByNameContains(String name, Pageable pageable) {
        return validationUnitRepository.findAllByNameContains(name, pageable);
    }

    @Transactional(readOnly = true)
    public List<ValidationUnit> findAllByLongNameContains(String name, Pageable pageable) {
        return validationUnitRepository.findAllByLongNameContains(name, pageable);
    }

    public boolean delete(long id) {
        try {
            validationUnitRepository.deleteById(id);
            return true;
        } catch (EmptyResultDataAccessException ignored) {
            return false;
        }
    }


}
