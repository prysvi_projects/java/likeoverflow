package com.quest.etna.repository;

import com.quest.etna.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, Long> {

    @Query("SELECT u FROM User u WHERE u.username=:username")
    User findByUsername(String username);

    List<User> findAllByCreationDateBetween(Date start, Date end);
    Page<User> findAllByEnabled(boolean enabled, Pageable pageable);
}
