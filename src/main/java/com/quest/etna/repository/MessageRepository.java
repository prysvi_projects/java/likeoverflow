package com.quest.etna.repository;

import com.quest.etna.model.Message;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.QueryHint;
import java.util.Date;
import java.util.List;

@Repository
public interface MessageRepository extends PagingAndSortingRepository<Message, Long> {

    @QueryHints({ @QueryHint(name = "org.hibernate.cacheable", value = "true") })
    @Query("SELECT t FROM Message t WHERE t.content LIKE CONCAT('%',:content,'%') order by t.content")
    List<Message> findAllByContentContainsText(String content, Pageable pageable);

    Page<Message> findAllByQuestionId(Long question, Pageable pageable);

    List<Message> findAllByCreationDateBetween(Date start, Date end);
}
