package com.quest.etna;

import com.quest.etna.config.AppProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;

@EnableCaching
@SpringBootApplication
@EnableConfigurationProperties(AppProperties.class)
public class QuestSpringBootApplication {

  public static void main(String[] args) {
    SpringApplication.run(QuestSpringBootApplication.class, args);
  }

}
