package com.quest.etna.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.security.crypto.encrypt.Encryptors;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

@ConfigurationProperties(prefix = "app")
public class AppProperties {
    private final Jwt jwt = new Jwt();
    private final OAuth2 oauth2 = new OAuth2();

    public Jwt getJwt() {
        return jwt;
    }

    public OAuth2 getOauth2() {
        return oauth2;
    }

    public static class Jwt {
        private String tokenSecret;
        private long tokenExpirationMsec;
        private String key;
        private String value;
        private String standard;

        public String getStandard() {
            return standard;
        }

        public void setStandard(String standard) {
            this.standard = standard;
        }

        public String getKey() {
            return Encryptors.text(standard, tokenSecret)
                    .decrypt(new String(new BigInteger(Base64.getDecoder().decode(key)).shiftRight(4).toByteArray(),
                            StandardCharsets.UTF_8));
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getValue() {
            return Encryptors.text(standard, tokenSecret)
                    .decrypt(new String(new BigInteger(Base64.getDecoder().decode(value)).shiftRight(4).toByteArray(),
                            StandardCharsets.UTF_8));
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getTokenSecret() {
            return tokenSecret;
        }

        public void setTokenSecret(String tokenSecret) {
            this.tokenSecret = tokenSecret;
        }

        public long getTokenExpirationMsec() {
            return tokenExpirationMsec;
        }

        public void setTokenExpirationMsec(long tokenExpirationMsec) {
            this.tokenExpirationMsec = tokenExpirationMsec;
        }
    }

    public static final class OAuth2 {
        private String tokenSuccessRedirectUrl;

        public String getTokenSuccessRedirectUrl() {
            return tokenSuccessRedirectUrl;
        }

        public void setTokenSuccessRedirectUrl(String tokenSuccessRedirectUrl) {
            this.tokenSuccessRedirectUrl = tokenSuccessRedirectUrl;
        }
    }
}
