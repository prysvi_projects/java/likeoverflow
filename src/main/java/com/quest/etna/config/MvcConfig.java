package com.quest.etna.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MvcConfig implements WebMvcConfigurer {
    @Value("${app.upload.upload-dir:uploads}")
    private String uploadDir;

    @Value("${app.upload.url-path:storage}")
    private String uploadUrlToDir;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/"+ uploadUrlToDir +"/**")
                .addResourceLocations("file://" + uploadDir + "/");
//        registry.addResourceHandler("/static/**")
//                .addResourceLocations("classpath:/toto/");
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("*")
                .allowCredentials(false)
                .maxAge(3600)
                .allowedHeaders("*")
                .exposedHeaders("X-Auth-Token", "Authorization","X-Authorization")
                .allowedMethods("*");
    }
}
