package com.quest.etna.config;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.quest.etna.mapper.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MapperConfig {

    @Bean
    public ObjectMapper getObjectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        return mapper;
    }

    @Bean
    public AddressMapper getAddressMapper() {
        return new AddressMapper();
    }

    @Bean
    public UserMapper getUserMapper() {
        return new UserMapper();
    }

    @Bean
    public TechnologyMapper getTechnologyMapper() {
        return new TechnologyMapper();
    }

    @Bean
    public PromotionMapper getPromotionMapper() {
        return new PromotionMapper();
    }

    @Bean
    public GradeMapper getGradeMapper() {
        return new GradeMapper();
    }

    @Bean
    public QuestionMapper getQuestionMapper() {
        return new QuestionMapper();
    }

    @Bean
    public MessageMapper getMessageMapper() {
        return new MessageMapper();
    }

    @Bean
    public UserMessageRatingMapper getUserMessageRatingMapper() {
        return new UserMessageRatingMapper();
    }

    @Bean
    public CommentMapper getCommentMapper() {
        return new CommentMapper();
    }

    @Bean
    public ChatMessageMapper getChatMessageMapper() {
        return new ChatMessageMapper();
    }

    @Bean
    public TimelineMapper getTimelineMapper() {
        return new TimelineMapper();
    }

    @Bean
    public UserPromoNoteMapper getUserPromoNoteMapper() {
        return new UserPromoNoteMapper();
    }

    @Bean
    public ValidationUnitActivityMapper getValidationUnitActivityMapper() {
        return new ValidationUnitActivityMapper();
    }

    @Bean
    public ValidationUnitMapper getValidationUnitMapper() {
        return new ValidationUnitMapper();
    }
}
