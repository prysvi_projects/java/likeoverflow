package com.quest.etna.mapper;

import com.quest.etna.dto.CommentDTO;
import com.quest.etna.model.Comment;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

public class CommentMapper extends AbstractMapper<CommentDTO, Comment> {
    public CommentMapper() {
        super(Mappers.getMapper(ICommentMapper.class));
    }

    @Mapper
    interface ICommentMapper extends IMapper<CommentDTO, Comment> {
    }
}