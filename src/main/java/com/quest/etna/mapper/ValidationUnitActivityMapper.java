package com.quest.etna.mapper;

import com.quest.etna.dto.ValidationUnitActivityDTO;
import com.quest.etna.model.ValidationUnitActivity;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

public class ValidationUnitActivityMapper extends AbstractMapper<ValidationUnitActivityDTO, ValidationUnitActivity> {
    public ValidationUnitActivityMapper() {
        super(Mappers.getMapper(IValidationUnitActivityMapper.class));
    }

    @Mapper
    interface IValidationUnitActivityMapper extends IMapper<ValidationUnitActivityDTO, ValidationUnitActivity> {
    }
}