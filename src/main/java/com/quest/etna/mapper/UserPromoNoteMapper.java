package com.quest.etna.mapper;

import com.quest.etna.dto.UserPromoNoteDTO;
import com.quest.etna.model.UserPromoNote;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

public class UserPromoNoteMapper extends AbstractMapper<UserPromoNoteDTO, UserPromoNote> {
    public UserPromoNoteMapper() {
        super(Mappers.getMapper(IUserPromoNoteMapper.class));
    }

    @Mapper
    interface IUserPromoNoteMapper extends IMapper<UserPromoNoteDTO, UserPromoNote> {
    }
}