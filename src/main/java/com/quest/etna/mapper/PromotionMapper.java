package com.quest.etna.mapper;

import com.quest.etna.dto.PromotionDTO;
import com.quest.etna.model.Promotion;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

public class PromotionMapper extends AbstractMapper<PromotionDTO, Promotion> {
    public PromotionMapper() {
        super(Mappers.getMapper(IPromotionMapper.class));
    }

    @Mapper
    interface IPromotionMapper extends IMapper<PromotionDTO, Promotion> {
    }
}