package com.quest.etna.mapper;

import com.quest.etna.dto.TechnologyDTO;
import com.quest.etna.model.Technology;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

public class TechnologyMapper extends AbstractMapper<TechnologyDTO, Technology> {
    public TechnologyMapper() {
        super(Mappers.getMapper(ITechnologyMapper.class));
    }

    @Mapper
    interface ITechnologyMapper extends IMapper<TechnologyDTO, Technology> {
    }
}