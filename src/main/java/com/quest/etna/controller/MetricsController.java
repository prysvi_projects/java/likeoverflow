package com.quest.etna.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.quest.etna.model.BaseModel;
import com.quest.etna.model.Message;
import com.quest.etna.model.User;
import com.quest.etna.service.MessageService;
import com.quest.etna.service.QuestionService;
import com.quest.etna.service.TechnologyService;
import com.quest.etna.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.text.DateFormatSymbols;
import java.util.*;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@RestController
@PreAuthorize("hasRole('ROLE_ADMIN')")
@RequestMapping("/metrics")
public class MetricsController {

    private final MessageService messageService;

    private final UserService userService;

    private final QuestionService questionService;

    private final TechnologyService technologyService;

    private final ObjectMapper objectMapper;

    @GetMapping("/messages/date/weeks/{count}")
    public HashMap<String, Integer> findAllMessagesInLastDateRange(@PathVariable Integer count) {
        HashMap<String, Integer> countByDay = new HashMap<>();
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.WEEK_OF_MONTH, -count);
        List<Message> result = messageService.findAllByCreationDateBetween(calendar.getTime(), new Date());
        result.forEach(message -> {
            String dayNameFromDate = getDayNameFromDate(message.getCreationDate());
            countByDay.put(dayNameFromDate, countByDay.getOrDefault(dayNameFromDate, 0) + 1);
        });
        return countByDay;
    }

    @GetMapping("/users/date/weeks/{count}")
    public HashMap<String, Integer> findAllUsersInLastDateRange(@PathVariable Integer count) {
        HashMap<String, Integer> countByDay = new HashMap<>();
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.WEEK_OF_MONTH, -count);
        List<User> result = userService.findAllByCreationDateBetween(calendar.getTime(), new Date());
        result.forEach(obj -> {
            String dayNameFromDate = getDayNameFromDate(obj.getCreationDate());
            countByDay.put(dayNameFromDate, countByDay.getOrDefault(dayNameFromDate, 0) + 1);
        });
        return countByDay;
    }

    @GetMapping("/questions/date/weeks/{count}")
    public HashMap<String, Integer> findAllQuestionsInLastDateRange(@PathVariable Integer count) {
        HashMap<String, Integer> countByDay = new HashMap<>();
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.WEEK_OF_MONTH, -count);
        List<? extends BaseModel> result = questionService.findAllByCreationDateBetween(calendar.getTime(), new Date());
        result.forEach(obj -> {
            String dayNameFromDate = getDayNameFromDate(obj.getCreationDate());
            countByDay.put(dayNameFromDate, countByDay.getOrDefault(dayNameFromDate, 0) + 1);
        });
        return countByDay;
    }

    @GetMapping("/users/active")
    public List<ObjectNode> findActiveUsersInLastDateRange(@RequestParam(required = false, defaultValue = "5") int size) {
        return userService.findAll(PageRequest.of(0, size, Sort.by(Sort.Direction.DESC, "messages"))).stream().map(obj -> {
            ObjectNode rootNode = objectMapper.createObjectNode();
            rootNode.put("id", obj.getId());
            rootNode.put("username", obj.getUsername());
            rootNode.put("count", obj.getMessages().size());
            return rootNode;
        }).collect(Collectors.toList());
    }

    @GetMapping("/technologies/popular")
    public List<ObjectNode> findMostPopularTechno(@RequestParam(required = false, defaultValue = "5") int size) {
        return technologyService.findAll(PageRequest.of(0, size, Sort.by(Sort.Direction.DESC, "users"))).stream().map(obj -> {
            ObjectNode rootNode = objectMapper.createObjectNode();
            rootNode.put("id", obj.getId());
            rootNode.put("name", obj.getName());
            rootNode.put("img", obj.getImgUrl());
            rootNode.put("count", obj.getUsers().size());
            return rootNode;
        }).collect(Collectors.toList());
    }


    public String getDayNameFromDate(Date date) {
        Calendar tmpCalendar = Calendar.getInstance();
        tmpCalendar.setTime(date);
        return getDayName(tmpCalendar.get(Calendar.DAY_OF_WEEK), new Locale("en", "GB"));
    }


    public String getDayName(int day, Locale locale) {
        //getDayName(dow, new Locale("da", "DK")
        DateFormatSymbols symbols = new DateFormatSymbols(locale);
        String[] dayNames = symbols.getWeekdays();
        return dayNames[day];
    }

}
