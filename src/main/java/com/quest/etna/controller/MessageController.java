package com.quest.etna.controller;

import com.quest.etna.dto.MessageDTO;
import com.quest.etna.dto.UserMessageRatingDTO;
import com.quest.etna.mapper.MessageMapper;
import com.quest.etna.mapper.UserMessageRatingMapper;
import com.quest.etna.model.*;
import com.quest.etna.security.SecurityProvider;
import com.quest.etna.service.MessageService;
import com.quest.etna.service.QuestionService;
import com.quest.etna.service.ReputationService;
import com.quest.etna.service.UserMessageRatingService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.HashMap;
import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("messages")
public class MessageController {

    private final UserMessageRatingService userMessageRatingService;
    private final UserMessageRatingMapper userMessageRatingMapper;
    private final MessageService messageService;
    private final MessageMapper messageMapper;
    private final QuestionService questionService;
    private final SecurityProvider securityProvider;
    private final ReputationService reputationService;

    @PostMapping("/question/{questionId}")
    @ResponseStatus(HttpStatus.CREATED)
    public MessageDTO create(@RequestBody MessageDTO dto, @PathVariable Long questionId) {
        Message fromDto = messageMapper.objFromDto(dto);
        fromDto.setUser(securityProvider.getAuthedUserModel());
        fromDto.setQuestion(questionService.findById(questionId, true));
        Message updatedMessage = messageService.create(fromDto);
        reputationService.refreshUserReputation(securityProvider.getAuthedUserModel(), 2);
        return messageMapper.objToDto(updatedMessage);
    }

    @PutMapping("{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN') or @messageService.findById(#id, true).getUser().getId()==authentication.id")
    public MessageDTO update(@RequestBody MessageDTO dto, @PathVariable Long id) {
        dto.setId(id);
        Message updatedMessage = messageService.update(messageMapper.objFromDto(dto));
        return messageMapper.objToDto(updatedMessage);
    }

    @GetMapping("{id}")
    public MessageDTO get(@PathVariable Long id) {
        return messageMapper.objToDto(messageService.findById(id, true));
    }

    @GetMapping("")
    public List<MessageDTO> findAll(@RequestParam(value = "perPage", required = false, defaultValue = "100") int perPage,
                                    @RequestParam(value = "page", required = false, defaultValue = "0") int page) {
        return messageMapper.objListToDtoList(messageService.findAll(PageRequest.of(page, perPage, Sort.by("creationDate"))));
    }

    @GetMapping("/question/{questionId}")
    public Page<MessageDTO> findAllByQuestion(@PathVariable Long questionId,
                                              @RequestParam(value = "sortDir", required = false, defaultValue = "ASC") Sort.Direction sortDir,
                                              @RequestParam(value = "sortField", required = false, defaultValue = "creationDate") String sortField,
                                              @RequestParam(value = "perPage", required = false, defaultValue = "100") int perPage,
                                              @RequestParam(value = "page", required = false, defaultValue = "0") int page) {

        return messageService.findAllByQuestionPage(questionId,
                PageRequest.of(page, perPage, Sort.by(sortDir, sortField)))
                .map(messageMapper::objToDto);
    }

    @GetMapping("/_search")
    public List<MessageDTO> findAllByContent(@RequestParam(value = "perPage", required = false, defaultValue = "100") int perPage,
                                             @RequestParam(value = "page", required = false, defaultValue = "0") int page,
                                             @RequestParam(value = "keyword", required = true) String keyword) {
        return messageMapper.objListToDtoList(messageService.findAllByContent(keyword, PageRequest.of(page, perPage)));
    }

    @DeleteMapping("{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN') or @messageService.findById(#id, true).getUser().getId()==authentication.id")
    public ResponseEntity<?> delete(@PathVariable long id) {
        if (messageService.findById(id, true).getQuestion().getQuestionMessage().getId() == id) {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "You cant remove question message");
        }
        HashMap<String, Boolean> response = new HashMap<>() {
            {
                put("success", messageService.delete(id));
            }
        };
        return ResponseEntity.ok(response);
    }

    @PostMapping("/{messageId}/rate")
    public UserMessageRatingDTO rateMessage(@RequestParam String reaction, @PathVariable Long messageId) {
        Reaction userReaction = Reaction.valueOf(reaction);
        Message message = messageService.findById(messageId, true);
        User authedUser = securityProvider.getAuthedUserModel();
        securityProvider.checkEnoughReputationOrRise(2);
        reputationService.refreshUserReputation(authedUser, -1);
        if (message.getUser() != null) {
            if (userReaction == Reaction.DOWN) {
                reputationService.refreshUserReputation(authedUser, -2);
            } else {
                reputationService.refreshUserReputation(authedUser, 5);
            }
        }
        UserMessageRating rating = userMessageRatingService.findByIdOrCreate(new UserMessageRatingId(authedUser.getId(),
                message.getId()));
        rating.setUser(authedUser);
        rating.setUserId(authedUser.getId());
        rating.setMessage(message);
        rating.setMessageId(message.getId());
        rating.setReaction(userReaction);
        UserMessageRatingDTO result = userMessageRatingMapper.objToDto(userMessageRatingService.saveOrUpdate(rating));
        messageService.update(message.updateRatingScore());
        return result;
    }
}
