package com.quest.etna.controller;

import com.quest.etna.dto.AddressDTO;
import com.quest.etna.mapper.AddressMapper;
import com.quest.etna.model.Address;
import com.quest.etna.security.SecurityProvider;
import com.quest.etna.service.AddressService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


@RestController
@RolesAllowed({"ROLE_USER", "ROLE_ADMIN"})
@RequestMapping("/address")
@RequiredArgsConstructor
public class AddressController {

    private final AddressService addressService;

    private final AddressMapper addressMapper;

    private final SecurityProvider securityProvider;

    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    public AddressDTO create(@RequestBody AddressDTO addressDTO) {
        Address address = addressMapper.objFromDto(addressDTO);
        address.setUser(securityProvider.getAuthedUserModel());
        return addressMapper.objToDto(addressService.create(address));
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("@addressService.findById(#id, true).getUser().getId()==authentication.id or hasRole('ROLE_ADMIN')")
    public AddressDTO update(@RequestBody AddressDTO addressDTO, @PathVariable Integer id) {
        addressDTO.setId(id);
        Address address = addressMapper.objFromDto(addressDTO);
        address.setUser(securityProvider.getAuthedUserModel());
        return addressMapper.objToDto(addressService.update(address, false));
    }

    @GetMapping("")
    @ResponseStatus(HttpStatus.OK)
    public ArrayList<AddressDTO> getAll(
            @RequestParam(value = "perPage", required = false, defaultValue = "100") int perPage,
            @RequestParam(value = "page", required = false, defaultValue = "0") int page
    ) {
        List<Address> foundAddresses;
        if (securityProvider.authedHasRole("ROLE_ADMIN")) {
            foundAddresses = addressService.findAll(PageRequest.of(page, perPage));
        } else {
            foundAddresses = addressService.findAllByUserId(securityProvider.getAuthedUserModel().getId(),
                    PageRequest.of(page, perPage));
        }
        return addressMapper.objListToDtoList(foundAddresses);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("@addressService.findById(#id, true).getUser().getId()==authentication.id or hasRole('ROLE_ADMIN')")
    public AddressDTO getById(@PathVariable int id) {
        return addressMapper.objToDto(addressService.findById(id, true));
    }

    @GetMapping("/users/{id}")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("authentication.id == #id or hasRole('ROLE_ADMIN')")
    public ArrayList<AddressDTO> getByUserId(
            @RequestParam(value = "perPage", required = false, defaultValue = "100") int perPage,
            @RequestParam(value = "page", required = false, defaultValue = "0") int page,
            @PathVariable int id) {
        return addressMapper.objListToDtoList(addressService.findAllByUserId(id, PageRequest.of(page, perPage)));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("@addressService.findById(#id, true).getUser().getId()==authentication.id or hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> delete(@PathVariable int id) {
        HashMap<String, Boolean> response = new HashMap<>() {
            {
                put("success", addressService.delete(id));
            }
        };
        return ResponseEntity.ok(response);
    }
}
