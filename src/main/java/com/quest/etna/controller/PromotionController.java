package com.quest.etna.controller;

import com.quest.etna.dto.PromotionDTO;
import com.quest.etna.mapper.PromotionMapper;
import com.quest.etna.service.PromotionService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("promotions")
public class PromotionController {

    private final PromotionService promotionService;

    private final PromotionMapper promotionMapper;

    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public PromotionDTO create(@RequestBody PromotionDTO dto) {
        return promotionMapper.objToDto(promotionService.create(promotionMapper.objFromDto(dto)));
    }

    @PutMapping("{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public PromotionDTO update(@RequestBody PromotionDTO dto, @PathVariable Long id) {
        dto.setId(id);
        return promotionMapper.objToDto(promotionService.update(promotionMapper.objFromDto(dto)));
    }

    @GetMapping("{id}")
    public PromotionDTO get(@PathVariable Long id) {
        return promotionMapper.objToDto(promotionService.findById(id, true));
    }

    @GetMapping("")
    public List<PromotionDTO> findAll(@RequestParam(value = "perPage", required = false, defaultValue = "100") int perPage,
                                      @RequestParam(value = "page", required = false, defaultValue = "0") int page) {
        return promotionMapper.objListToDtoList(promotionService.findAll(PageRequest.of(page, perPage)));
    }

    @GetMapping("/_search")
    public List<PromotionDTO> findAllByName(@RequestParam(value = "perPage", required = false, defaultValue = "100") int perPage,
                                            @RequestParam(value = "page", required = false, defaultValue = "0") int page,
                                            @RequestParam(value = "keyword", required = true) String keyword) {
        return promotionMapper.objListToDtoList(promotionService.findAllByName(keyword, PageRequest.of(page, perPage)));
    }

    @DeleteMapping("{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> delete(@PathVariable int id) {
        HashMap<String, Boolean> response = new HashMap<>() {
            {
                put("success", promotionService.delete(id));
            }
        };
        return ResponseEntity.ok(response);
    }
}
