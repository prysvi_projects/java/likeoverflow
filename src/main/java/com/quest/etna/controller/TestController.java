package com.quest.etna.controller;

import com.quest.etna.model.*;
import com.quest.etna.model.etna.EtnaUser;
import com.quest.etna.service.*;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RequiredArgsConstructor
@RestController
@PreAuthorize("hasRole('ROLE_ADMIN')")
@RequestMapping("/test")
public class TestController {
    private final UserService userService;
    private final QuestionService questionService;
    private final MessageService messageService;
    private final CommentService commentService;
    private final ChatMessageService chatMessageService;

    @GetMapping("/users")
    public List<User> users() {
        List<User> all = userService.findAll(Pageable.unpaged());
        return all;
    }

    @GetMapping("/questions")
    public List<Question> questions() {
        List<Question> content = questionService.findAll(Pageable.unpaged()).getContent();
        return content;
    }

    @GetMapping("/messages")
    public List<Message> messages() {
        List<Message> all = messageService.findAll(Pageable.unpaged());
        return all;
    }

    @GetMapping("/comments")
    public List<Comment> comments() {
        List<Comment> all = commentService.findAll(Pageable.unpaged());
        return all;
    }

    @GetMapping("/chatMessages")
    public List<ChatMessage> chatMessages() {
        List<ChatMessage> all = chatMessageService.findAll(Pageable.unpaged());
        return all;
    }
}
