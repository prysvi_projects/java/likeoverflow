package com.quest.etna.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserPromoNoteId implements Serializable {
    Long userId;
    Long promoId;
}
