package com.quest.etna.model;

public enum ValidationStatus {
    VALIDATED,AWAIT,REFUSED
}
