package com.quest.etna.model;

import com.quest.etna.annotation.ReflectionUpdate;
import com.quest.etna.model.etna.EtnaPromo;
import lombok.*;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@EqualsAndHashCode(callSuper = false, of = {"id"})
@AllArgsConstructor
@NoArgsConstructor
@ToString(exclude = {"users", "questions"})
@Data
@Entity
public class Promotion extends BaseModel {

    @Column(name = "learning_start")
    protected Date learningStart;
    @Column(name = "learning_end")
    protected Date learningEnd;
    //TODO set Manually
    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, unique = true)
    private String name;
    @ReflectionUpdate(action = ReflectionUpdate.Action.IGNORE)
    @OneToMany(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinColumn(name = "promotion", referencedColumnName = "id")
    private List<User> users;
    @ReflectionUpdate(action = ReflectionUpdate.Action.IGNORE)
    @OneToMany(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinColumn(name = "promotion", referencedColumnName = "id")
    private List<Question> questions;

    public Promotion(EtnaPromo etnaPromo) {
        this.id = etnaPromo.getId();
        if (etnaPromo.getSpe() != null && etnaPromo.getSpe().length() > 0) {
            this.name = etnaPromo.getSpe() + " - " + etnaPromo.getWall_name();
        } else {
            this.name = etnaPromo.getWall_name();
        }
        this.learningStart = etnaPromo.getLearning_start();
        this.learningEnd = etnaPromo.getLearning_end();
    }

    public Promotion(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Date getLastReloadAt() {
        return this.updatedDate != null ? this.updatedDate : this.creationDate;
    }

    public boolean isCanBeReloaded() {
        if (null == getLastReloadAt()) {
            return true;
        }
        return getCanReloadAfterAt().before(new Date());
    }

    public Date getCanReloadAfterAt() {
        if (null == getLastReloadAt()) {
            return new Date();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(getLastReloadAt());
        calendar.add(Calendar.HOUR, 3);
        return calendar.getTime();
    }
}
