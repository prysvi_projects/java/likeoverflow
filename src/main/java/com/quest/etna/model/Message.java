package com.quest.etna.model;

import lombok.*;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;
@ToString(exclude = {"timelines", "comments", "userMessageRatings","question"})
@EqualsAndHashCode(callSuper = false)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Message extends BaseModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinColumn(name = "user", referencedColumnName = "id")
    private User user;

    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinColumn(name = "question", referencedColumnName = "id")
    private Question question;

    @Column(nullable = false, length = 65535, columnDefinition = "TEXT")
    private String content;

    @Column(name = "rating_score")
    private Long ratingScore = 0L;

    @OneToMany(cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
    @JoinColumn(name = "message", referencedColumnName = "id")
    @Fetch(value = FetchMode.SUBSELECT)
    private List<UserMessageRating> userMessageRatings;

    @OneToMany(cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
    @JoinColumn(name = "message", referencedColumnName = "id")
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Comment> comments;


    @OneToMany(cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
    @JoinColumn(name = "message", referencedColumnName = "id")
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Timeline> timelines;


    public Message(User user, Question question, String content, List<UserMessageRating> userMessageRatings, List<Comment> comments) {
        this.user = user;
        this.question = question;
        this.content = content;
        this.userMessageRatings = userMessageRatings;
        this.comments = comments;
    }


    public Long getRatingScore() {
        updateRatingScore();
        return ratingScore;
    }

    @Override
    public void preUpdateFunction() {
        super.preUpdateFunction();
        this.updateRatingScore();
    }

    @Override
    public void prePersistFunction() {
        super.prePersistFunction();
        this.updateRatingScore();
    }

    public Message updateRatingScore() {
        if (userMessageRatings == null || userMessageRatings.size() == 0) {
            this.ratingScore = 0L;
            return this;
        }
        long down = userMessageRatings.stream()
                .filter(userMessageRating -> userMessageRating.getReaction() == Reaction.DOWN).count();
        long up = userMessageRatings.stream()
                .filter(userMessageRating -> userMessageRating.getReaction() == Reaction.UP).count();
        this.ratingScore = up - down;
        return this;
    }
}