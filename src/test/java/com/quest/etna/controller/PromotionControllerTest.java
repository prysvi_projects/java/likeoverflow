package com.quest.etna.controller;

import com.fasterxml.jackson.databind.MapperFeature;
import com.quest.etna.dto.PromotionDTO;
import com.quest.etna.mapper.PromotionMapper;
import com.quest.etna.model.Promotion;
import com.quest.etna.model.User;
import com.quest.etna.model.UserRole;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("testing")
class PromotionControllerTest extends AbstractControllerTest {
    private static User refUser;
    private static User refAdmin;
    private static Promotion refPromotion;
    private String userToken;
    private String adminToken;

    @Autowired
    private PromotionMapper mapper;

    @BeforeAll
    public void init() {
        testObjectMapper.disable(MapperFeature.USE_ANNOTATIONS);
        initDataBase("clean.sql");
        initDataBase("users_only.sql");
        rootControllerUrl = "/promotions";
        refUser = new User(1L, "user", "secret", UserRole.ROLE_USER);
        userToken = generateAuthToken(refUser);
        refAdmin = new User(2L, "admin", "secret", UserRole.ROLE_ADMIN);
        adminToken = generateAuthToken(refAdmin);
    }

    @AfterAll
    public void destroy() {
        initDataBase("clean.sql");
    }

    @Test
    @Order(1)
    @DisplayName("FAIL: [403][TOKEN] User can create promotion")
    public void user_create_promotion_fail() throws Exception {
        PromotionDTO promotionDTO = new PromotionDTO();
        promotionDTO.setName("Promotion2021");
        this.mockMvc.perform(post(rootControllerUrl)
                .header("Authorization", "Bearer " + userToken)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(promotionDTO)))
                .andExpect(status().isForbidden())
                .andDo(print());
    }

    @Test
    @Order(2)
    @DisplayName("OK: [201][TOKEN] Admin can create promotion")
    public void admin_create_promotion_ok() throws Exception {
        PromotionDTO promotionDTO = new PromotionDTO();
        promotionDTO.setName("Promotion");
        promotionDTO.setId(11L);
        String result = this.mockMvc.perform(post(rootControllerUrl)
                .header("Authorization", "Bearer " + adminToken)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(promotionDTO)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name").value(promotionDTO.getName()))
                .andDo(print())
                .andReturn().getResponse().getContentAsString();
        refPromotion = mapper.objFromDto(testObjectMapper.readValue(result, PromotionDTO.class));
    }

    @Test
    @Order(3)
    @DisplayName("OK: [200][TOKEN] Admin can update promotion")
    public void admin_patch_promotion_ok() throws Exception {
        PromotionDTO promotionDTO = new PromotionDTO();
        promotionDTO.setId(refPromotion.getId());
        promotionDTO.setName("PromotionChanged");
        String result = this.mockMvc.perform(put(rootControllerUrl + "/" + refPromotion.getId())
                .header("Authorization", "Bearer " + adminToken)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(promotionDTO)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(promotionDTO.getName()))
                .andDo(print())
                .andReturn().getResponse().getContentAsString();
        refPromotion = mapper.objFromDto(testObjectMapper.readValue(result, PromotionDTO.class));
    }

    @Test
    @Order(4)
    @DisplayName("FAIL: [403][TOKEN] User can't update promotion")
    public void user_patch_promotion_fail() throws Exception {
        PromotionDTO promotionDTO = new PromotionDTO();
        promotionDTO.setId(refPromotion.getId());
        promotionDTO.setName("PromotionChanged");
        this.mockMvc.perform(put(rootControllerUrl + "/" + refPromotion.getId())
                .header("Authorization", "Bearer " + userToken)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(promotionDTO)))
                .andExpect(status().isForbidden())
                .andDo(print());
    }


    @Test
    @Order(5)
    @DisplayName("OK: [200][TOKEN] User / Admin can get promotion")
    public void user_admin_get_promotion_ok() throws Exception {
        this.mockMvc.perform(get(rootControllerUrl)
                .header("Authorization", "Bearer " + userToken))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(1)))
                .andDo(print());

        this.mockMvc.perform(get(rootControllerUrl)
                .header("Authorization", "Bearer " + adminToken))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(1)))
                .andDo(print());
    }

    @Test
    @Order(6)
    @DisplayName("OK: [200][TOKEN] User / Admin can search promotion by name")
    public void user_admin_find_by_name_promotion_ok() throws Exception {
        this.mockMvc.perform(get(rootControllerUrl+"/_search")
                .param("keyword","Promotion2021")
                .header("Authorization", "Bearer " + userToken))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(0)))
                .andDo(print());

        this.mockMvc.perform(get(rootControllerUrl+"/_search")
                .param("keyword","Promotion")
                .header("Authorization", "Bearer " + userToken))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(1)))
                .andDo(print());

        this.mockMvc.perform(get(rootControllerUrl+"/_search")
                .param("keyword","Promotion2021")
                .header("Authorization", "Bearer " + adminToken))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(0)))
                .andDo(print());

        this.mockMvc.perform(get(rootControllerUrl+"/_search")
                .param("keyword","Promotion")
                .header("Authorization", "Bearer " + adminToken))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(1)))
                .andDo(print());
    }

    @Test
    @Order(7)
    @DisplayName("FAIL: [403][TOKEN] User Delete promotion")
    public void delete_stranger_address_403() throws Exception {
        this.mockMvc.perform(delete(rootControllerUrl+"/"+refPromotion.getId())
                .header("Authorization", "Bearer " + userToken))
                .andExpect(status().isForbidden());
    }

    @Test
    @Order(8)
    @DisplayName("OK: [200][TOKEN] Admin Delete promotion")
    public void admin_delete_stranger_address_OK() throws Exception {
        this.mockMvc.perform(delete(rootControllerUrl+"/"+refPromotion.getId())
                .header("Authorization", "Bearer " + adminToken))
                .andExpect(status().isOk());
    }
}