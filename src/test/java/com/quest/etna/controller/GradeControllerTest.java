package com.quest.etna.controller;

import com.fasterxml.jackson.databind.MapperFeature;
import com.quest.etna.dto.GradeDTO;
import com.quest.etna.mapper.GradeMapper;
import com.quest.etna.model.Grade;
import com.quest.etna.model.User;
import com.quest.etna.model.UserRole;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("testing")
class GradeControllerTest extends AbstractControllerTest {
    private static User refUser;
    private static User refAdmin;
    private static Grade refGrade;
    private String userToken;
    private String adminToken;

    @Autowired
    private GradeMapper mapper;

    @BeforeAll
    public void init() {
        testObjectMapper.disable(MapperFeature.USE_ANNOTATIONS);
        initDataBase("clean.sql");
        initDataBase("db.sql");
        rootControllerUrl = "/grades";
        refUser = new User(1L, "user", "secret", UserRole.ROLE_USER);
        userToken = generateAuthToken(refUser);
        refAdmin = new User(2L, "admin", "secret", UserRole.ROLE_ADMIN);
        adminToken = generateAuthToken(refAdmin);
    }

    @AfterAll
    public void destroy() {
        initDataBase("clean.sql");
    }

    @Test
    @Order(1)
    @DisplayName("FAIL: [403][TOKEN] User cant create grade")
    public void user_create_grade_ok() throws Exception {
        GradeDTO gradeDTO = new GradeDTO();
        gradeDTO.setImgUrl("randImgUrl1");
        gradeDTO.setName("Sergant");
        String result = this.mockMvc.perform(post(rootControllerUrl)
                .header("Authorization", "Bearer " + userToken)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(gradeDTO)))
                .andExpect(status().isForbidden())
                .andDo(print())
                .andReturn().getResponse().getContentAsString();
    }

    @Test
    @Order(2)
    @DisplayName("OK: [201][TOKEN] Admin can create grade")
    public void admin_create_grade_ok() throws Exception {
        GradeDTO gradeDTO = new GradeDTO();
        gradeDTO.setImgUrl("randImgUrl");
        gradeDTO.setName("Sergant");
        String result = this.mockMvc.perform(post(rootControllerUrl)
                .header("Authorization", "Bearer " + adminToken)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(gradeDTO)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name").value(gradeDTO.getName()))
                .andExpect(jsonPath("$.imgUrl").value(gradeDTO.getImgUrl()))
                .andDo(print())
                .andReturn().getResponse().getContentAsString();
        refGrade = mapper.objFromDto(testObjectMapper.readValue(result, GradeDTO.class));
    }

    @Test
    @Order(3)
    @DisplayName("OK: [200][TOKEN] Admin can update grade")
    public void admin_patch_grade_ok() throws Exception {
        GradeDTO gradeDTO = new GradeDTO();
        gradeDTO.setId(refGrade.getId());
        gradeDTO.setImgUrl("randImgUrlChanged");
        gradeDTO.setName("SergantChanged");
        String result = this.mockMvc.perform(put(rootControllerUrl + "/" + refGrade.getId())
                .header("Authorization", "Bearer " + adminToken)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(gradeDTO)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(gradeDTO.getName()))
                .andExpect(jsonPath("$.imgUrl").value(gradeDTO.getImgUrl()))
                .andDo(print())
                .andReturn().getResponse().getContentAsString();
        refGrade = mapper.objFromDto(testObjectMapper.readValue(result, GradeDTO.class));
    }

    @Test
    @Order(4)
    @DisplayName("FAIL: [403][TOKEN] User can't update grade")
    public void user_patch_grade_fail() throws Exception {
        GradeDTO gradeDTO = new GradeDTO();
        gradeDTO.setId(refGrade.getId());
        gradeDTO.setImgUrl("randImgUrlChanged");
        gradeDTO.setName("SergantChanged");
        this.mockMvc.perform(put(rootControllerUrl + "/" + refGrade.getId())
                .header("Authorization", "Bearer " + userToken)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(gradeDTO)))
                .andExpect(status().isForbidden())
                .andDo(print());
    }


    @Test
    @Order(5)
    @DisplayName("OK: [200][TOKEN] User / Admin can get grade")
    public void user_admin_get_grade_ok() throws Exception {
        this.mockMvc.perform(get(rootControllerUrl)
                .header("Authorization", "Bearer " + userToken))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(1)))
                .andDo(print());

        this.mockMvc.perform(get(rootControllerUrl)
                .header("Authorization", "Bearer " + adminToken))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(1)))
                .andDo(print());
    }

    @Test
    @Order(6)
    @DisplayName("OK: [200][TOKEN] User / Admin can search grade by name")
    public void user_admin_find_by_name_grade_ok() throws Exception {
        this.mockMvc.perform(get(rootControllerUrl+"/_search")
                .param("keyword","Sergant")
                .header("Authorization", "Bearer " + userToken))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(1)))
                .andDo(print());

        this.mockMvc.perform(get(rootControllerUrl+"/_search")
                .param("keyword","Sergant")
                .header("Authorization", "Bearer " + userToken))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(1)))
                .andDo(print());

        this.mockMvc.perform(get(rootControllerUrl+"/_search")
                .param("keyword","Sergant")
                .header("Authorization", "Bearer " + adminToken))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(1)))
                .andDo(print());

        this.mockMvc.perform(get(rootControllerUrl+"/_search")
                .param("keyword","Sergant")
                .header("Authorization", "Bearer " + adminToken))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(1)))
                .andDo(print());
    }

    @Test
    @Order(7)
    @DisplayName("FAIL: [403][TOKEN] User Delete grade")
    public void delete_stranger_address_403() throws Exception {
        this.mockMvc.perform(delete(rootControllerUrl+"/"+refGrade.getId())
                .header("Authorization", "Bearer " + userToken))
                .andExpect(status().isForbidden());
    }

    @Test
    @Order(8)
    @DisplayName("OK: [200][TOKEN] Admin Delete grade")
    public void admin_delete_stranger_address_OK() throws Exception {
        this.mockMvc.perform(delete(rootControllerUrl+"/"+refGrade.getId())
                .header("Authorization", "Bearer " + adminToken))
                .andExpect(status().isOk());
    }
}