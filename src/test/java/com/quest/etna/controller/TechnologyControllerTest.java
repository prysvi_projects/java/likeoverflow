package com.quest.etna.controller;

import com.fasterxml.jackson.databind.MapperFeature;
import com.quest.etna.dto.TechnologyDTO;
import com.quest.etna.mapper.TechnologyMapper;
import com.quest.etna.model.Technology;
import com.quest.etna.model.User;
import com.quest.etna.model.UserRole;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("testing")
class TechnologyControllerTest extends AbstractControllerTest {
    private static User refUser;
    private static User refAdmin;
    private static Technology refTechnology;
    private String userToken;
    private String adminToken;

    @Autowired
    private TechnologyMapper mapper;

    @BeforeAll
    public void init() {
        testObjectMapper.disable(MapperFeature.USE_ANNOTATIONS);
        initDataBase("clean.sql");
        initDataBase("db.sql");
        rootControllerUrl = "/technologies";
        refUser = new User(1L, "user", "secret", UserRole.ROLE_USER);
        userToken = generateAuthToken(refUser);
        refAdmin = new User(2L, "admin", "secret", UserRole.ROLE_ADMIN);
        adminToken = generateAuthToken(refAdmin);
    }

    @AfterAll
    public void destroy() {
        initDataBase("clean.sql");
    }

    @Test
    @Order(1)
    @DisplayName("FAIL: [403][TOKEN] User can create technology")
    public void user_create_technology_fail() throws Exception {
        TechnologyDTO technologyDTO = new TechnologyDTO();
        technologyDTO.setImgUrl("randImgUrl1");
        technologyDTO.setName("Java1");
        this.mockMvc.perform(post(rootControllerUrl)
                .header("Authorization", "Bearer " + userToken)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(technologyDTO)))
                .andExpect(status().isForbidden())
                .andDo(print());
    }

    @Test
    @Order(2)
    @DisplayName("OK: [201][TOKEN] Admin can create technology")
    public void admin_create_technology_ok() throws Exception {
        TechnologyDTO technologyDTO = new TechnologyDTO();
        technologyDTO.setImgUrl("randImgUrl");
        technologyDTO.setName("Java");
        String result = this.mockMvc.perform(post(rootControllerUrl)
                .header("Authorization", "Bearer " + adminToken)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(technologyDTO)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name").value(technologyDTO.getName()))
                .andExpect(jsonPath("$.imgUrl").value(technologyDTO.getImgUrl()))
                .andDo(print())
                .andReturn().getResponse().getContentAsString();
        refTechnology = mapper.objFromDto(testObjectMapper.readValue(result, TechnologyDTO.class));
    }

    @Test
    @Order(3)
    @DisplayName("OK: [200][TOKEN] Admin can update technology")
    public void admin_patch_technology_ok() throws Exception {
        TechnologyDTO technologyDTO = new TechnologyDTO();
        technologyDTO.setId(refTechnology.getId());
        technologyDTO.setImgUrl("randImgUrlChanged");
        technologyDTO.setName("JavaChanged");
        String result = this.mockMvc.perform(put(rootControllerUrl + "/" + refTechnology.getId())
                .header("Authorization", "Bearer " + adminToken)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(technologyDTO)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(technologyDTO.getName()))
                .andExpect(jsonPath("$.imgUrl").value(technologyDTO.getImgUrl()))
                .andDo(print())
                .andReturn().getResponse().getContentAsString();
        refTechnology = mapper.objFromDto(testObjectMapper.readValue(result, TechnologyDTO.class));
    }

    @Test
    @Order(4)
    @DisplayName("FAIL: [403][TOKEN] User can't update technology")
    public void user_patch_technology_fail() throws Exception {
        TechnologyDTO technologyDTO = new TechnologyDTO();
        technologyDTO.setId(refTechnology.getId());
        technologyDTO.setImgUrl("randImgUrlChanged");
        technologyDTO.setName("JavaChanged");
        this.mockMvc.perform(put(rootControllerUrl + "/" + refTechnology.getId())
                .header("Authorization", "Bearer " + userToken)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(technologyDTO)))
                .andExpect(status().isForbidden())
                .andDo(print());
    }


    @Test
    @Order(5)
    @DisplayName("OK: [200][TOKEN] User / Admin can get technology")
    public void user_admin_get_technology_ok() throws Exception {
        this.mockMvc.perform(get(rootControllerUrl)
                .header("Authorization", "Bearer " + userToken))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(1)))
                .andDo(print());

        this.mockMvc.perform(get(rootControllerUrl)
                .header("Authorization", "Bearer " + adminToken))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(1)))
                .andDo(print());
    }

    @Test
    @Order(6)
    @DisplayName("OK: [200][TOKEN] User / Admin can search technology by name")
    public void user_admin_find_by_name_technology_ok() throws Exception {
        this.mockMvc.perform(get(rootControllerUrl+"/_search")
                .param("keyword","Java1")
                .header("Authorization", "Bearer " + userToken))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(0)))
                .andDo(print());

        this.mockMvc.perform(get(rootControllerUrl+"/_search")
                .param("keyword","Java")
                .header("Authorization", "Bearer " + userToken))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(1)))
                .andDo(print());

        this.mockMvc.perform(get(rootControllerUrl+"/_search")
                .param("keyword","Java1")
                .header("Authorization", "Bearer " + adminToken))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(0)))
                .andDo(print());

        this.mockMvc.perform(get(rootControllerUrl+"/_search")
                .param("keyword","Java")
                .header("Authorization", "Bearer " + adminToken))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(1)))
                .andDo(print());
    }

    @Test
    @Order(7)
    @DisplayName("FAIL: [403][TOKEN] User Delete technology")
    public void delete_stranger_address_403() throws Exception {
        this.mockMvc.perform(delete(rootControllerUrl+"/"+refTechnology.getId())
                .header("Authorization", "Bearer " + userToken))
                .andExpect(status().isForbidden());
    }

    @Test
    @Order(8)
    @DisplayName("OK: [200][TOKEN] Admin Delete technology")
    public void admin_delete_stranger_address_OK() throws Exception {
        this.mockMvc.perform(delete(rootControllerUrl+"/"+refTechnology.getId())
                .header("Authorization", "Bearer " + adminToken))
                .andExpect(status().isOk());
    }
}