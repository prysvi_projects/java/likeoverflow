package com.quest.etna.controller;

import com.fasterxml.jackson.databind.MapperFeature;
import com.quest.etna.dto.MessageDTO;
import com.quest.etna.mapper.MessageMapper;
import com.quest.etna.model.Message;
import com.quest.etna.model.Question;
import com.quest.etna.model.User;
import com.quest.etna.model.UserRole;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("testing")
class MessagesControllerTest extends AbstractControllerTest {
    private static User refUser;
    private static User refUser2;
    private static User refAdmin;
    private static Question refQuestion;
    private static Message refMessage;
    private String userToken;
    private String userToken2;
    private String adminToken;

    @Autowired
    private MessageMapper mapper;

    @BeforeAll
    public void init() {
        testObjectMapper.disable(MapperFeature.USE_ANNOTATIONS);
        initDataBase("clean.sql");
        initDataBase("message.sql");
        rootControllerUrl = "/messages";
        refUser = new User(1L, "user", "secret", UserRole.ROLE_USER);
        userToken = generateAuthToken(refUser);
        refAdmin = new User(2L, "admin", "secret", UserRole.ROLE_ADMIN);
        adminToken = generateAuthToken(refAdmin);
        refUser2 = new User(3L, "user_2", "secret", UserRole.ROLE_USER);
        userToken2 = generateAuthToken(refUser2);
        refQuestion = new Question();
        refQuestion.setId(6L);
    }

    @AfterAll
    public void destroy() {
        initDataBase("clean.sql");
    }

//    @Test
//    @Order(1)
//    @DisplayName("FAIL: [403][TOKEN] User cant create message")
//    public void user_create_message_ok() throws Exception {
//        MessageDTO messageDTO = new MessageDTO();
//        messageDTO.setContent("randImgUrl1");
//        String result = this.mockMvc.perform(post(rootControllerUrl)
//                .header("Authorization", "Bearer " + userToken)
//                .contentType(MediaType.APPLICATION_JSON)
//                .content(objectMapper.writeValueAsString(messageDTO)))
//                .andExpect(status().isForbidden())
//                .andDo(print())
//                .andReturn().getResponse().getContentAsString();
//    }

    @Test
    @Order(2)
    @DisplayName("OK: [201][TOKEN] User can create message")
    public void create_message_ok() throws Exception {
        MessageDTO messageDTO = new MessageDTO();
        messageDTO.setContent("some random content");
        String result = this.mockMvc.perform(post(rootControllerUrl+"/question/"+refQuestion.getId())
                .header("Authorization", "Bearer " + userToken)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(messageDTO)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.content").value(messageDTO.getContent()))
                .andDo(print())
                .andReturn().getResponse().getContentAsString();
        refMessage = mapper.objFromDto(testObjectMapper.readValue(result, MessageDTO.class));
    }

    @Test
    @Order(3)
    @DisplayName("OK: [200][TOKEN] User can update message")
    public void admin_patch_message_ok() throws Exception {
        MessageDTO messageDTO = mapper.objToDto(refMessage);
        messageDTO.setContent("Changed Content");
        String result = this.mockMvc.perform(put(rootControllerUrl + "/" + refMessage.getId())
                .header("Authorization", "Bearer " + userToken)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(messageDTO)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content").value(messageDTO.getContent()))
                .andReturn().getResponse().getContentAsString();
        refMessage = mapper.objFromDto(testObjectMapper.readValue(result, MessageDTO.class));
    }

    @Test
    @Order(4)
    @DisplayName("FAIL: [403][TOKEN] User can't update strange message")
    public void user_patch_message_fail() throws Exception {
        MessageDTO messageDTO = mapper.objToDto(refMessage);
        messageDTO.setContent("asdasdasd");
        this.mockMvc.perform(put(rootControllerUrl + "/" + refMessage.getId())
                .header("Authorization", "Bearer " + userToken2)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(messageDTO)))
                .andExpect(status().isForbidden())
                .andDo(print());
    }


    @Test
    @Order(5)
    @DisplayName("OK: [200][TOKEN] User  can get all messages")
    public void user_admin_get_message_ok() throws Exception {
        this.mockMvc.perform(get(rootControllerUrl)
                .header("Authorization", "Bearer " + userToken))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(2)));

    }

    @Test
    @Order(6)
    @DisplayName("OK: [200][TOKEN] User can search message by content")
    public void user_admin_find_by_name_message_ok() throws Exception {
        this.mockMvc.perform(get(rootControllerUrl + "/_search")
                .param("keyword", refMessage.getContent())
                .header("Authorization", "Bearer " + userToken))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(1)))
                .andDo(print());
    }

    @Test
    @Order(7)
    @DisplayName("OK: [200][TOKEN] User can search message by question")
    public void user_admin_find_by_question_message_ok() throws Exception {
        this.mockMvc.perform(get(rootControllerUrl + "/question/"+refQuestion.getId())
                .param("keyword", refMessage.getContent())
                .header("Authorization", "Bearer " + userToken))
                .andExpect(jsonPath("$.content").isArray())
                .andExpect(jsonPath("$.content", hasSize(1)))
                .andDo(print());
    }

    @Test
    @Order(8)
    @DisplayName("FAIL: [403][TOKEN] User Delete strange message")
    public void delete_stranger_address_403() throws Exception {
        this.mockMvc.perform(delete(rootControllerUrl + "/" + refMessage.getId())
                .header("Authorization", "Bearer " + userToken2))
                .andExpect(status().isForbidden());
    }

    @Test
    @Order(9)
    @DisplayName("OK: [200][TOKEN] User Delete own message")
    public void user_delete_own_address_OK() throws Exception {
        this.mockMvc.perform(delete(rootControllerUrl + "/" + refMessage.getId())
                .header("Authorization", "Bearer " + userToken))
                .andExpect(status().isOk());
    }
}