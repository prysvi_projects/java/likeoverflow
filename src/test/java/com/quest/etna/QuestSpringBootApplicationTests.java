package com.quest.etna;

import org.junit.jupiter.api.Test;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.security.crypto.encrypt.TextEncryptor;
import org.springframework.security.crypto.keygen.KeyGenerators;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

//@SpringBootTest
class QuestSpringBootApplicationTests {


    public void encrypt() {
        TextEncryptor cryptor = Encryptors.text("HS256", "923ca7d7ae214937");
        String textToHide = "";
        String byBase = Base64.getEncoder().encodeToString(new BigInteger(cryptor.encrypt(textToHide).getBytes(StandardCharsets.UTF_8)).shiftLeft(4).toByteArray());
        System.out.println("Crypted Result :"+byBase);
    }

    public void decrypt() {
        TextEncryptor cryptor = Encryptors.text("HS256", "923ca7d7ae214937");
        String textToDecode = "";
        String fromCryptor = cryptor.decrypt(new String(new BigInteger(Base64.getDecoder().decode(textToDecode)).shiftRight(4).toByteArray(), StandardCharsets.UTF_8));
        System.out.println("DECrypted Result :"+fromCryptor);
    }

}
